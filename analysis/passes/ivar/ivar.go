package ivar

import (
	"golang.org/x/tools/go/analysis"
	"golang.org/x/tools/go/analysis/passes/inspect"
)

// Analyzer returns the ivar (immutable variable) linter instance.
func Analyzer() *analysis.Analyzer {
	return &analysis.Analyzer{
		Name:     "ivar",
		Doc:      `Enforce certain variables are immutable`,
		Requires: []*analysis.Analyzer{inspect.Analyzer},
		Run:      run,
	}
}

//nolint:nilnil // returning nil with no error is expected from this function
func run(_ *analysis.Pass) (any, error) {
	return nil, nil
}
